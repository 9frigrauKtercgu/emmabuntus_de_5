- Créer ce lien suite à cette erreur :

cp: impossible d'évaluer '/usr/share/live/build/data/debian-cd/bullseye/amd64_netinst_udeb_include': Aucun fichier ou dossier de ce type

ou

sed: impossible de lire exclude: Aucun fichier ou dossier de ce type

se mettre dans :  cd /usr/share/live/build/data/debian-cd

sudo ln -s squeeze ./bookworm

- erreur :

cp: impossible d'évaluer 'chroot/boot/memtest86+.bin': Aucun fichier ou dossier de ce type

editer sudo geany /usr/lib/live/build/binary_memtest

Remplacer

Check_package chroot /boot/memtest86+.bin memtest86+

par

Check_package chroot /boot/memtest86+x32.bin memtest86+
Check_package chroot /boot/memtest86+x64.bin memtest86+

Remplacer

    true)
        cp -a chroot/boot/${LB_MEMTEST}.bin "${DESTDIR}"/memtest
        ;;

    false)
        cp -a /boot/${LB_MEMTEST}.bin "${DESTDIR}"/memtest
        ;;

par

    true)
        cp -a chroot/boot/${LB_MEMTEST}x32.bin "${DESTDIR}"/memtest
        cp -a chroot/boot/${LB_MEMTEST}x64.bin "${DESTDIR}"/memtest
        ;;

    false)
        cp -a /boot/${LB_MEMTEST}x32.bin "${DESTDIR}"/memtest
        cp -a /boot/${LB_MEMTEST}x64.bin "${DESTDIR}"/memtest
        ;;



- erreur :

Package firmware-linux is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'firmware-linux' has no installation candidate

editer  :

sudo geany /usr/lib/live/build/installer_debian-installer
sudo geany /usr/lib/live/build/chroot_firmware

Remplacer

            FIRMWARE_PACKAGES="${FIRMWARE_PACKAGES} firmware-linux"

par

            FIRMWARE_PACKAGES="${FIRMWARE_PACKAGES}"


