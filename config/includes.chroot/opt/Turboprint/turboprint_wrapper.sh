#! /bin/bash

# turboprint_wrapper.sh --
#
#   This file permits to install TurboPrint for the Emmabuntüs Equitable Distribs,
#   because there are a bug to install normaly
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

nom_distribution="Emmabuntüs Equitable"

nom_logiciel_affichage="Turboprint"
nom_paquet=turboprint
nom_logiciel_exe=turboprint

nom_script=install_turboprint.sh

dir_install=/opt/Turboprint
delai_fenetre=20


#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi

if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]]
then

    echo "# $nom_logiciel_affichage is already installed"

    $nom_logiciel_exe

    exit 0

fi



export WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_demarrage_soft_non_libre'" | sed "s%\\\%%g"</input>
</text>

<hbox spacing="10" space-expand="false" space-fill="false">
<button cancel></button>
<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>gtk-ok</label>
<action>exit:OK</action>
</button>
</hbox>

</vbox>
</window>'

MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE="$(gtkdialog --center --program=WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE)"

eval ${MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE}


if [[ ${EXIT} != "OK" ]]
then

    echo "# $nom_logiciel_affichage software installation canceled"

    exit 0

else

    pkexec $dir_install/$nom_script

    if [[ $(which $nom_logiciel_exe) ]]
    then
        $nom_logiciel_exe
    else
        zenity --error --timeout=$delai_fenetre \
            --text="<span color=\"red\">$install_ko</span>"
    fi

fi





