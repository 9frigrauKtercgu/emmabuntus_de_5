#! /bin/bash

# install_steam.sh --
#
#   This file permits to install nofree softwares for the Emmabuntüs Equitable Distribs.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="Steam"

nom_paquet=steam

base_distribution="bookworm"

dir_install_non_free_softwares=/opt/Install_non_free_softwares
delai_fenetre=20


#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi



(

#Désactivation de Gmenu de Cairo-dock pendant l'installation de logiciel pour éviter les fenêtres de notifications
/usr/bin/cairo_dock_gmenu_off.sh


# non-free-codecs

/opt/Depots/install_depot_non_free.sh


# Suppression de la surveillance de la mise à jour des paquets
pkill pk-update-icon

echo "# $msg_repository_update"
sudo apt-get -qq update

) |
zenity --progress --pulsate \
  --title="$install_title" \
  --text="$install_text" \
  --width=500 \
  --percentage=0 --auto-close --no-cancel
    if [ $? = "1" ]
    then
      sudo dpkg --configure -a

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

      zenity --error --timeout=$delai_fenetre \
        --text="$install_cancel"
    else
      sudo dpkg --configure -a
      echo "# Install Completed"
    fi


(

echo "# $msg_install $nom_logiciel_affichage, $thank_waiting"

if [[ $(uname -m | grep -e "x86_64") ]] ; then

    #Commande pour forcer l'acquitement automatique de la license
    echo steam steam/question select "I AGREE" | sudo /usr/bin/debconf-set-selections
    echo steam steam/license note '' | sudo /usr/bin/debconf-set-selections

    xterm -T "Install Steam" -e "sudo apt install -y -q ${nom_paquet}"

fi

if [[ $(uname -m | grep -e "x86_64") ]] ; then
    ${nom_paquet}
fi


#Activation de Gmenu de Cairo-dock
/usr/bin/cairo_dock_gmenu_on.sh

echo "100" ;

) |
zenity --progress --pulsate \
  --title="$install_title" \
  --text="$install_text" \
  --width=300 \
  --percentage=0 --auto-close --no-cancel
    if [ $? = "1" ]
    then

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

      zenity --error --timeout=$delai_fenetre \
        --text="$install_cancel"
    else
      echo "# Install Completed"
    fi






