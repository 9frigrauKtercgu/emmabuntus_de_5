#! /bin/bash

# install_microsoft_fonts.sh --
#
#   This file permits to install nofree Microsoft Fonts for the Emmabuntüs Equitable Distribs.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

dir_source_fonts=/opt/Install_non_free_softwares
source_fonts=fonts_MS_Office.tar.gz
dir_install_fonts=/usr/share/fonts/truetype/msttcorefonts


if test -f ${dir_source_fonts}/${source_fonts}
then

    if ! test -d ${dir_install_fonts}
    then
        sudo mkdir ${dir_install_fonts}
    fi

    sudo chmod -R a+rX ${dir_install_fonts}

    if  test -d ${dir_install_fonts}
    then
        sudo cp  ${dir_source_fonts}/${source_fonts} ${dir_install_fonts}

        cd ${dir_install_fonts}

        sudo tar xvf ${source_fonts}

        sudo rm ${source_fonts}

        fc-cache -fv

        echo "Install OK"
    else
        echo "Install KO !!!"
    fi

else
    echo "Install KO !!!"
fi

