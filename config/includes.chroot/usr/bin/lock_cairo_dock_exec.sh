#! /bin/bash

# lock_cairo_dock_exec.sh --
#
#   This file permits to init cairo-dock for Emmabuntüs distribution
#
#   Created on 2010-22 by Collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

#var
cmd_lock_dock=$1

env_emmabuntus=$2


if [[ ${cmd_lock_dock} == "1" ]]
then

    sed s/"^Lock_Dock=\"[0-9]*\""/"Lock_Dock=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

    cp ${env_emmabuntus}.tmp ${env_emmabuntus}
    rm ${env_emmabuntus}.tmp

else

    if [[ ${cmd_lock_dock} == "0" ]]
    then

        sed s/"^Lock_Dock=\"[0-9]*\""/"Lock_Dock=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    fi
fi
