#!/bin/bash

# emmabuntus_config_dock.sh --
#
#   This file permits to
#    - active or not the dock for Emmabuntüs (Emmabuntus_enable_dock.sh)
#    - lock or unlock mechanism of the cairo-dock (lock_cairo_dock.sh)
#    - active or not the XFCE taskbar for (Emmabuntus_enable_taskbar.sh)
#    - active or not the XFCE worskpace
#    - active or not Clipboard manager Qlipper for XFCE or LXQt
#    - active ou not XFCE sound events
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config_desktop=${repertoire_emmabuntus}/init_config_dock.txt
file_redshift=~/.config/autostart/redshift-gtk.desktop
file_clipboard_manager_xfce=~/.config/autostart/lxqt-qlipper.desktop
file_clipboard_manager_lxqt=~/.config/autostart/lxqt-qlipper.desktop
file_gtk2=~/.gtkrc-2.0
file_gtk3=~/.config/gtk-3.0/settings.ini
dir_theme=/usr/share/themes
name_dark_theme=Qogir-dark-largerborders
name_light_theme=Qogir-light-largerborders
auto_upgrade_file=/etc/apt/apt.conf.d/20auto-upgrades


Init=""
Init=$1
echo "Init=$Init"

# Valeurs initiale de la fenêtre
enable_dock=true
lock_dock=false
name_dock=""
enable_taskbar=false
enable_workspace=false
restart_panel=false
enable_redshift=false
enable_clipboard_manager=false
enable_dark_theme=true
enable_autoupgrade=false
enable_sound_events=false
mode_live=false

# Détermination de la configuration
if [[ ${Init} == ""  ]]
then
    DOCK_CONFIG_VISIBLE=true
    BUTTON_CANCEL_VISIBLE=false
    AUTOUPGRADE_VISIBLE=true
else
    DOCK_CONFIG_VISIBLE=false
    BUTTON_CANCEL_VISIBLE=true
    AUTOUPGRADE_VISIBLE=true
fi

if ps -A | grep "xfce4-session" ; then
    XFCE_CONFIG_VISIBLE=true
else
    XFCE_CONFIG_VISIBLE=false
fi

if ps -A | grep "xfce4-session" ; then
    name_dock="Dock_XFCE"
elif ps -A | grep "lxsession" ; then
    name_dock="Dock_LXDE"
elif ps -A | grep "openbox" ; then
    name_dock="Dock_OpenBox"
elif ps -A | grep "lxqt-session" ; then
    name_dock="Dock_LXQT"
fi


# Détermination du mode live
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then
    mode_live=true
else
    mode_live=false
fi

if ${mode_live} == true ; then
    AUTOUPGRADE_VISIBLE=false
fi


# Détermination des config actives


if [[ $(cat ${env_emmabuntus} | grep ${name_dock} | grep "1") ]] ; then
    enable_dock=true
else
    enable_dock=false
fi

if ps -A | grep "xfce4-session" ; then

    if [[ $(xfconf-query -c xfce4-panel -p /plugins/plugin-9) == separator ]] ; then
        enable_taskbar=false
    else
        enable_taskbar=true
    fi

    if [[ $(xfconf-query -c xfce4-panel -p /plugins/plugin-10) == separator ]] ; then
        enable_workspace=false
    else
        enable_workspace=true
    fi

    if [[ $(xfconf-query -c xsettings -p /Net/EnableEventSounds) == true ]] ; then
        enable_sound_events=true
    else
        enable_sound_events=false
    fi

fi

if ps -A | grep "xfce4-session" ; then
    if [[ $(cat ${file_redshift} | grep "X-GNOME-Autostart-enabled=true") ]] ; then
        enable_redshift=true
    else
        enable_redshift=false
    fi
else
    enable_redshift=false
fi

if ps -A | grep "xfce4-session" ; then
    if [[ $(cat ${file_clipboard_manager_xfce} | grep "Hidden=false") ]] ; then
        enable_clipboard_manager=true
    else
        enable_clipboard_manager=false
    fi
elif ps -A | grep "lxqt-session" ; then
    if [[ $(cat ${file_clipboard_manager_lxqt} | grep "Hidden=false") ]] ; then
        enable_clipboard_manager=true
    else
        enable_clipboard_manager=false
    fi
fi

if cat ${auto_upgrade_file} | grep "APT::Periodic::Update-Package-Lists" | grep "1" ; then
    enable_autoupgrade=true
else
    enable_autoupgrade=false
fi

source /usr/bin/emmabuntus_found_theme.sh

highlight_color=$(FOUND_HIGHLIGHT_COLOR)
name_dark_theme=$(FOUND_THEME_DARK)
name_light_theme=$(FOUND_THEME_LIGHT)


if [[ ${highlight_color} == blue ]] ; then
    enable_dark_theme=false
else
    enable_dark_theme=true
fi

# Changement du nom du fichier_init_config_desktop par session
if ps -A | grep "xfce4-session" ; then
    fichier_init_config_desktop=${repertoire_emmabuntus}/xfce_init_config_dock.txt
elif ps -A | grep "lxsession" ; then
    fichier_init_config_desktop=${repertoire_emmabuntus}/lxde_init_config_dock.txt
elif ps -A | grep "openbox" ; then
    fichier_init_config_desktop=${repertoire_emmabuntus}/openbox_init_config_dock.txt
elif ps -A | grep "lxqt-session" ; then
    fichier_init_config_desktop=${repertoire_emmabuntus}/lxqt_init_config_dock.txt
fi


if [[ ${Init} != ""  || ! ( -f $fichier_init_config_desktop) ]]
then

URL_dock="\<span color=\'${highlight_color}\'>Dock\</span>"

if [[ ${Init} == ""  ]] && [[ ${mode_live} == false ]]
then
        nom_titre_window="$(eval_gettext 'Desktop configuration (continued)')"
else
        nom_titre_window="$(eval_gettext 'Desktop configuration')"
fi

message_info="\n\
Cairo-Dock ${URL_dock} $(eval_gettext 'Configuration (Application Launcher):')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Information:')\</span> $(eval_gettext 'During training sessions or public presentations,     ')\n\
$(eval_gettext 'we recommend you lock the dock.')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Information:')\</span> $(eval_gettext 'You can change these settings later on, by going to:  ')\n\
$(eval_gettext 'Application menu -> Cairo-Dock.')"


URL_panel="\<span color=\'${highlight_color}\'>$(eval_gettext 'Panel')\</span>"

message_panel="\n\
$(eval_gettext 'Dashboard configuration') (${URL_panel}):     "


if [[ ${Init} == ""  ]]
then

message_theme_info="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Information:')\</span> $(eval_gettext 'You can change these settings later on, by going to:  ')\n\
$(eval_gettext 'Application menu -> Settings -> Emmabuntüs - Desktop.')"

else

message_theme_info=""

fi

message_theme="\n\
$(eval_gettext 'System configuration:')"


# Message pour infos complémentaires

comment_info_enable_dock="$(eval_gettext 'Activate the dock (Application Launcher at the bottom of the screen), allowing beginners to use the computer more easily')"
comment_info_lock_dock="$(eval_gettext 'Locks the dock to prevent unwanted changes')"
comment_info_taskbar="$(eval_gettext 'The taskbar is located in the banner at the top of the screen, and allows you to quickly switch between applications')"
comment_info_workspace="$(eval_gettext 'The workspace selector is located in the banner at the top of the screen, and allows you to quickly switch between desktops')"
comment_info_redshift="$(eval_gettext 'Eye protection against blue light. You can go to the') ~/.config/redshift.conf $(eval_gettext 'file to set different color temperatures between day and night')"
comment_info_clipboard_manager="$(eval_gettext 'Allows to have a clipboard manager accessible in the system tray')"
comment_info_dark_theme="$(eval_gettext 'The dark theme is less tiring for your eyes')"
comment_info_accessibility_theme="$(eval_gettext 'Accessibility enhanced theme with 3 pixel window borders')"
comment_info_sound_events="$(eval_gettext 'Allows you to have sound notifications when emails arrive, tasks are completed, etc.')"
comment_info_autoupgrade="$(eval_gettext 'Allows the security packages to be automatically updated')"


    export WINDOW_DIALOG='<window title="'${nom_titre_window}'" icon-name="gtk-dialog-question" deletable="false" resizable="false">
    <vbox spacing="0" space-fill="true" space-expand="true">
    <text use-markup="true" wrap="false" xalign="0" justify="3" visible="'${DOCK_CONFIG_VISIBLE}'" tooltip-text="'${comment_info}'">
    <input>echo "'$message_info'" | sed "s%\\\%%g"</input>
    </text>

    <hseparator space-expand="false" space-fill="false"></hseparator>

    <checkbox height_request="30" yalign="1" active="'${enable_dock}'" visible="'${DOCK_CONFIG_VISIBLE}'" tooltip-text="'${comment_info_enable_dock}'">
    <variable>enable_dock</variable>
    <label>"'$(eval_gettext 'Activate the dock')'"</label>
    </checkbox>
    <checkbox height_request="30" yalign="1" active="'${lock_dock}'" visible="'${DOCK_CONFIG_VISIBLE}'" tooltip-text="'${comment_info_lock_dock}'">
    <variable>lock_dock</variable>
    <label>"'$(eval_gettext 'Lock the Emmabuntüs default dock')'"</label>
    </checkbox>

   <hseparator height_request="5" space-expand="true" space-fill="true" visible="'${DOCK_CONFIG_VISIBLE}'"></hseparator>

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_panel'" | sed "s%\\\%%g"</input>
    </text>


    <checkbox height_request="30" yalign="1" active="'${enable_taskbar}'" tooltip-text="'${comment_info_taskbar}'" visible="'${XFCE_CONFIG_VISIBLE}'">
    <variable>enable_taskbar</variable>
    <label>"'$(eval_gettext 'Activate the taskbar')'"</label>
    </checkbox>
    <checkbox height_request="30" yalign="1" active="'${enable_workspace}'" tooltip-text="'${comment_info_workspace}'" visible="'${XFCE_CONFIG_VISIBLE}'">
    <variable>enable_workspace</variable>
    <label>"'$(eval_gettext 'Activate the workspace selector')'"</label>
    </checkbox>
    <checkbox height_request="30" yalign="1" active="'${enable_redshift}'" tooltip-text="'${comment_info_redshift}'" visible="'${XFCE_CONFIG_VISIBLE}'">
    <variable>enable_redshift</variable>
    <label>"'$(eval_gettext 'Activate RedShift')'"</label>
    </checkbox>
    <checkbox height_request="30" yalign="1" active="'${enable_clipboard_manager}'" tooltip-text="'${comment_info_clipboard_manager}'">
    <variable>enable_clipboard_manager</variable>
    <label>"'$(eval_gettext 'Activate the clipboard manager')'"</label>
    </checkbox>

    <hseparator height_request="5" space-expand="true" space-fill="true"></hseparator>

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_theme'" | sed "s%\\\%%g"</input>
    </text>

    <checkbox height_request="30" yalign="1" active="'${enable_dark_theme}'" tooltip-text="'${comment_info_dark_theme}'">
    <variable>enable_dark_theme</variable>
    <label>"'$(eval_gettext 'Activate a dark theme')'"</label>
    </checkbox>

    <checkbox height_request="30" yalign="1" active="'${enable_sound_events}'" tooltip-text="'${comment_info_sound_events}'" visible="'${XFCE_CONFIG_VISIBLE}'">
    <variable>enable_sound_events</variable>
    <label>"'$(eval_gettext 'Activate sound events')'"</label>
    </checkbox>

    <checkbox height_request="30" yalign="1" active="'${enable_autoupgrade}'" tooltip-text="'${comment_info_autoupgrade}'" visible="'${AUTOUPGRADE_VISIBLE}'">
    <variable>enable_autoupgrade</variable>
    <label>"'$(eval_gettext 'Enable automatic security updates')'"</label>
    </checkbox>

    <hseparator height_request="5" space-expand="true" space-fill="true" visible="'${DOCK_CONFIG_VISIBLE}'"></hseparator>

    <text use-markup="true" wrap="false" xalign="0" justify="3" visible="'${DOCK_CONFIG_VISIBLE}'">
    <input>echo "'$message_theme_info'" | sed "s%\\\%%g"</input>
    </text>

    <hseparator space-expand="false" space-fill="false"></hseparator>

    <hbox space-expand="false" space-fill="false">
    <button visible="'${BUTTON_CANCEL_VISIBLE}'">
    <input file icon="gtk-no"></input>
    <label>"'$(eval_gettext 'Cancel')'"</label>
    <action>exit:exit</action>
    </button>
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>
    </vbox>
    </window>'


    MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

    eval ${MENU_DIALOG}
    echo "MENU_DIALOG=${MENU_DIALOG}"

    if [ ${EXIT} == "OK" ]
    then

        echo "Config desktop" > ${fichier_init_config_desktop}

        # Gestion protection dock
        if [[ ${Init} == ""  ]]
        then
            if [ ${lock_dock} == "true" ]
            then
                /usr/bin/lock_cairo_dock_exec.sh 1 ${env_emmabuntus}
            else
                /usr/bin/lock_cairo_dock_exec.sh 0 ${env_emmabuntus}
            fi
        fi

       # Gestion barre des tâches

       if ps -A | grep "xfce4-session" ; then

            if [ ${enable_taskbar} == "true" ]
             then

                if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-9 | grep "tasklist") ]] ; then
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "tasklist"

                    if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-9/grouping | grep [0-1]) ]] ; then
                        xfconf-query -v -c xfce4-panel -p /plugins/plugin-9/grouping -n -t int -s 1
                    fi

                    restart_panel=true

                fi


                echo "Enable XCFE taskbar" >> ${fichier_init_config_desktop}

             else

                if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-9 | grep "separator") ]] ; then
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "separator"

                    restart_panel=true

                fi


                echo "Disable XCFE taskbar" >> ${fichier_init_config_desktop}

             fi
        fi

        # Gestion activation espace de travail

       if ps -A | grep "xfce4-session" ; then

            if [ ${enable_workspace} == "true" ] ; then

                if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-10 | grep "showdesktop") ]] ; then
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-10 -s "showdesktop"
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-11 -s "pager"

                    restart_panel=true

                fi

                echo "Enable XCFE Workspace" >> ${fichier_init_config_desktop}

            else

                if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-10 | grep "separator") ]] ; then
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-10 -s "separator"
                    xfconf-query -v -c xfce4-panel -p /plugins/plugin-11 -s "separator"

                    restart_panel=true

            fi


                echo "Disable XCFE Workspace" >> ${fichier_init_config_desktop}

            fi

        fi

        # Gestion activation dock

        if [[ ${Init} == ""  ]]
        then
            if [ ${enable_dock} == "true" ]
             then
                if test -f ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf
                then
                    rm ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf
                fi

                sed s/"^${name_dock}=\"[0-9]*\""/"${name_dock}=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

                cp ${env_emmabuntus}.tmp ${env_emmabuntus}
                rm ${env_emmabuntus}.tmp

                echo "Enable Emmabuntüs Dock" >> ${fichier_init_config_desktop}

             else
             # Si pas de dock activation par défaut de la barre des tâches

                if ps -A | grep "xfce4-session" ; then

                    if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-9 | grep "tasklist") ]] ; then
                        xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "tasklist"

                        if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-9/grouping | grep [0-1]) ]] ; then
                            xfconf-query -v -c xfce4-panel -p /plugins/plugin-9/grouping -n -t int -s 1
                        fi

                        restart_panel=true

                    fi

                fi

                sed s/"^${name_dock}=\"[0-9]*\""/"${name_dock}=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

                cp ${env_emmabuntus}.tmp ${env_emmabuntus}
                rm ${env_emmabuntus}.tmp

                echo "Disable Emmabuntüs Dock" >> ${fichier_init_config_desktop}
            fi
        fi

        # Gestion activation RedShift

        if [ ${enable_redshift} == "true" ]
        then

            echo "Enable RedShift" >> ${fichier_init_config_desktop}

            if [[ $(cat ${file_redshift} | grep "X-GNOME-Autostart-enabled=false") ]] ; then

                sed s/^X-GNOME-Autostart-enabled=false/X-GNOME-Autostart-enabled=true/ ${file_redshift} > ${file_redshift}.tmp
                sed s/^Hidden=[^$]*/Hidden=false/ ${file_redshift}.tmp > ${file_redshift}
                rm ${file_redshift}.tmp

                redshift-gtk &

            fi

         else

            echo "Disable RedShift" >> ${fichier_init_config_desktop}

            if [[ $(cat ${file_redshift} | grep "X-GNOME-Autostart-enabled=true") ]] ; then

                sed s/^X-GNOME-Autostart-enabled=true/X-GNOME-Autostart-enabled=false/ ${file_redshift} > ${file_redshift}.tmp
                sed s/^Hidden=[^$]*/Hidden=true/ ${file_redshift}.tmp > ${file_redshift}
                rm ${file_redshift}.tmp

                pkill redshift-gtk

            fi

        fi

        # Gestion activation gestionnaire de presse-papiers

        if [ ${enable_clipboard_manager} == "true" ]
        then

            if ps -A | grep "xfce4-session" ; then

                echo "Enable Clipboard manager XFCE" >> ${fichier_init_config_desktop}

                if [[ $(cat ${file_clipboard_manager_xfce} | grep "Hidden=true") ]] ; then

                    sed s/^Hidden=true/Hidden=false/ ${file_clipboard_manager_xfce} > ${file_clipboard_manager_xfce}.tmp
                    cp ${file_clipboard_manager_xfce}.tmp ${file_clipboard_manager_xfce}
                    rm ${file_clipboard_manager_xfce}.tmp

                    qlipper &

                fi
            elif ps -A | grep "lxqt-session" ; then

                echo "Enable Clipboard manager LXQt" >> ${fichier_init_config_desktop}

                if [[ $(cat ${file_clipboard_manager_lxqt} | grep "Hidden=true") ]] ; then

                    sed s/^Hidden=true/Hidden=false/ ${file_clipboard_manager_lxqt} > ${file_clipboard_manager_lxqt}.tmp
                    cp ${file_clipboard_manager_lxqt}.tmp ${file_clipboard_manager_lxqt}
                    rm ${file_clipboard_manager_lxqt}.tmp

                    qlipper &

                fi
            fi


         else

            if ps -A | grep "xfce4-session" ; then

                echo "Disable Clipboard manager XFCE" >> ${fichier_init_config_desktop}

                if [[ $(cat ${file_clipboard_manager_xfce} | grep "Hidden=false") ]] ; then

                    sed s/^Hidden=false/Hidden=true/ ${file_clipboard_manager_xfce} > ${file_clipboard_manager_xfce}.tmp
                    cp ${file_clipboard_manager_xfce}.tmp ${file_clipboard_manager_xfce}
                    rm ${file_clipboard_manager_xfce}.tmp

                    pkill qlipper

                fi
            elif ps -A | grep "lxqt-session" ; then

                echo "Disable Clipboard manager LXQt" >> ${fichier_init_config_desktop}

                if [[ $(cat ${file_clipboard_manager_lxqt} | grep "Hidden=false") ]] ; then

                    sed s/^Hidden=false/Hidden=true/ ${file_clipboard_manager_lxqt} > ${file_clipboard_manager_lxqt}.tmp
                    cp ${file_clipboard_manager_lxqt}.tmp ${file_clipboard_manager_lxqt}
                    rm ${file_clipboard_manager_lxqt}.tmp

                    pkill qlipper

                fi
            fi

        fi

        # Gestion des événements sonores

        if ps -A | grep "xfce4-session" ; then

            if [ ${enable_sound_events} == "true" ]
            then

                echo "Enable Sound events" >> ${fichier_init_config_desktop}

                xfconf-query -c xsettings -p /Net/EnableEventSounds -s true
                xfconf-query -c xsettings -p /Net/EnableInputFeedbackSounds -s true

            else

                echo "Disable Sound events" >> ${fichier_init_config_desktop}

                xfconf-query -c xsettings -p /Net/EnableEventSounds -s false
                xfconf-query -c xsettings -p /Net/EnableInputFeedbackSounds -s false

            fi

        fi


        # Test si l'utilisateur fait parti du groupe sudo
        if [[ $(groups | grep sudo) != "" ]] ; then

            # Gestion mises à jour automatique de sécurité
        if [[ ${enable_autoupgrade} == "true" && $(cat ${auto_upgrade_file} | grep "APT::Periodic::Update-Package-Lists" | grep "0") ]] ;  then

            echo "Enable autoupgrade" >> ${fichier_init_config_desktop}

            pkexec /usr/bin/emmabuntus_auto_upgrade_exec.sh "1"

        elif [[ ${enable_autoupgrade} == "false" && $(cat ${auto_upgrade_file} | grep "APT::Periodic::Update-Package-Lists" | grep "1") ]] ;  then

            echo "Disable autoupgrade" >> ${fichier_init_config_desktop}

            pkexec /usr/bin/emmabuntus_auto_upgrade_exec.sh "0"

        fi

        fi


        # Gestion theme sombre
        #/etc/lightdm/lightdm-gtk-greeter.conf
        #.config/gtk-3.0/settings.ini
        #.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml
        #.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml
        #.gtkrc-2.0

        if [ ${enable_dark_theme} == "true" ]
        then

            name_theme=${name_dark_theme}

            echo "Enable Dark theme" >> ${fichier_init_config_desktop}

         else

            name_theme=${name_light_theme}

            echo "Disable Dark theme" >> ${fichier_init_config_desktop}
        fi

        if ps -A | grep "lxqt-session" ; then

            echo "Activation theme LXQt"

            sed s/"^gtk-theme-name[^$]*"/"gtk-theme-name = \"${name_theme}\""/ ${file_gtk2} > ${file_gtk2}.tmp
            cp ${file_gtk2}.tmp ${file_gtk2}
            rm ${file_gtk2}.tmp

            sed s/"^gtk-theme-name[^$]*"/"gtk-theme-name=${name_theme}"/ ${file_gtk3} > ${file_gtk3}.tmp
            cp ${file_gtk3}.tmp ${file_gtk3}
            rm ${file_gtk3}.tmp

        fi


        if test -d ${dir_theme}/${name_theme}/xfwm4 ; then
            xfconf-query -c xfwm4 -p /general/theme -s "${name_theme}"
        fi

        xfconf-query -c xsettings -p /Net/ThemeName -s "${name_theme}"

        # Test si l'utilisateur fait parti du groupe sudo
        if [[ $(groups | grep sudo) != "" ]] ; then
        pkexec /usr/bin/lightdm_gtk_greeter_exec.sh ${enable_dark_theme} ${name_dark_theme} ${name_light_theme}
        fi

    fi

    if [ ${restart_panel} == "true" ] ; then

        if ps -A | grep xfce4-panel
        then
            xfce4-panel --restart
        fi
        sleep 1

        if ps -A | grep xfce4-panel
        then
            echo ""
        else
            xfce4-panel &
        fi
    fi


fi
