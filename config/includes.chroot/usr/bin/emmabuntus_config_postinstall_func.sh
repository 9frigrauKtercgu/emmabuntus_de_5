#!/bin/bash


# Emmabuntus_config_postinstall_func.sh --
#
#   This file used to display a postinstall panel for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################


option=$1
choix_wallpaper=$2
#echo "choix_wallpaper=${choix_wallpaper}"

# Fichiers config économiseur d'écran
msg_old_image="Emmabuntüs"
msg_new_image1="Ubuntu"
msg_screensaver_off="Off"

# Fichiers config fond d'écran
repertoire_wallpaper_emmabuntus="/usr/share/xfce4/backdrops"

fond_ecran_en="framasoft_wallpaper_emmabuntus_classic"
fond_ecran_fr="framasoft_fond_ecran_accueil"



if [[ $LANG == fr* ]]
then
    fond_ecran=${fond_ecran_fr}
else
    fond_ecran=${fond_ecran_en}
fi




function Affichage_imagette_economiseur_ecran()
{
  if [[ $choix_wallpaper == *${msg_old_image}* ]]
  then
      wallpaper_file="${repertoire_wallpaper_emmabuntus}/screensaver/12m2 - Chambre.jpg"
  elif [[ $choix_wallpaper == *${msg_new_image1}* ]]
  then
      wallpaper_file=${repertoire_wallpaper_emmabuntus}/screen_ubuntu/Serenity_Enchanted_by_sirpecangum.jpg
  else
      wallpaper_file=/usr/share/pixmaps/Noir.jpg
  fi

  unlink ${lnk_imagette_economiseur_ecran}
  ln -sf "${wallpaper_file}" ${lnk_imagette_economiseur_ecran}

}


function Affichage_imagette_fond_ecran()
{
  if [[ $choix_wallpaper == *Classi* ]] ; then
      wallpaper_file=${repertoire_wallpaper_emmabuntus}/${fond_ecran}_4_3.jpg
  elif [[ $choix_wallpaper == *Emmabuntus-Ice* ]] ; then
      wallpaper_file=${repertoire_wallpaper_emmabuntus}/Emmabuntus_Ice_background.svg
  elif [[ $choix_wallpaper == *Emmabuntus-Blumine* ]] ; then
      wallpaper_file=${repertoire_wallpaper_emmabuntus}/Emmabuntus_logo_background_blumine.svg
  elif [[ $choix_wallpaper == *Wallpaper* ]] ; then
      wallpaper_file=${repertoire_wallpaper_emmabuntus}/${choix_wallpaper}-1280x960.svg
  else
      wallpaper_file=${repertoire_wallpaper_emmabuntus}/${fond_ecran_fr}.jpg
  fi

  unlink ${lnk_imagette_fond_ecran}
  ln -sf "${wallpaper_file}" ${lnk_imagette_fond_ecran}

}



if [[ ${option} == ECONOMISEUR_ECRAN ]] ; then
    Affichage_imagette_economiseur_ecran
fi

if [[ ${option} == FOND_ECRAN ]] ; then
    Affichage_imagette_fond_ecran
fi
