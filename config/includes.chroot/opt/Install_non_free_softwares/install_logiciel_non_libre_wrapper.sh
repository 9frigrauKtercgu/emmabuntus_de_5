#! /bin/bash

# install_logiciel_non_libre_wrapper.sh --
#
#   This file permits to install non-free softwares for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

nom_script=install_logiciel_non_libre.sh
nom_logiciel_affichage="Install non-free softwares"
dir_install_non_free_softwares=/opt/Install_non_free_softwares
utilisateur=emmabuntus
dir_install_fonts_arial=/usr/share/fonts/truetype/msttcorefonts/Arial.ttf
dir_install_fonts_calibri=/usr/share/fonts/truetype/fonts_MS_Office_2007/Calibri.ttf

if [[ $LANG == fr* ]]
then
    nom_logiciel_affichage="Installation des logiciels non libres"
fi


#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi


bureau=$XDG_DESKTOP_DIR


# Initilisation des variables du tableau
CODECS=false ; ARIAL_FONTS=false ; CALIBRI_FONTS=false
msg_codecs="Codecs"
ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
HEIGHT_WINDOW=400
HEIGHT_CHECKBOX=100


if [[ $(ls /home | grep "^${utilisateur}") ]] ; then

    if [[ $(uname -m | grep -e "x86_64") ]] ; then

        CODECS=true ; ARIAL_FONTS=true ; CALIBRI_FONTS=true
        ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
        HEIGHT_WINDOW=400
        HEIGHT_CHECKBOX=100

    else

        CODECS=true ; ARIAL_FONTS=true ; CALIBRI_FONTS=true
        ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
        HEIGHT_WINDOW=400
        HEIGHT_CHECKBOX=100

    fi

else

    if [[ $(uname -m | grep -e "x86_64") ]] ; then

        CODECS=false ; ARIAL_FONTS=false ; CALIBRI_FONTS=false
        ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
        HEIGHT_WINDOW=400
        HEIGHT_CHECKBOX=100

    else

        CODECS=false ; ARIAL_FONTS=false ; CALIBRI_FONTS=false
        ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
        HEIGHT_WINDOW=400
        HEIGHT_CHECKBOX=100

    fi

fi

# Test de l'installation des paquets pour désactiver la visibilité de ceux-ci
CHECKBOX_CODECS_ACTIF="true"
CHECKBOX_ARIAL_FONTS_ACTIF="true"
CHECKBOX_CALIBRI_FONTS_ACTIF="true"


nom_paquet=libdvdcss2
if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]] ; then
    CHECKBOX_CODECS_ACTIF="false" ; CODECS="true"
fi

if test -f ${dir_install_fonts_arial} ; then
    CHECKBOX_ARIAL_FONTS_ACTIF="false" ; ARIAL_FONTS="true"
fi

if test -f ${dir_install_fonts_calibri} ; then
    CHECKBOX_CALIBRI_FONTS_ACTIF="false" ; CALIBRI_FONTS="true"
fi

if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    echo "Internet is a live"
    CHECKBOX_INTERNET_ACTIF="true"
else
    echo "Internet wasn't a live !!"
    CHECKBOX_INTERNET_ACTIF="false"
fi


export WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" height_request="'${HEIGHT_WINDOW}'" maximize_initially="true" resizable="false">
<vbox spacing="0" space-fill="true" space-expand="true">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_demarrage_soft_non_libre_all'" | sed "s%\\\%%g"</input>
</text>

<hseparator space-expand="true" space-fill="true"></hseparator>

<vbox homogeneous="true" height_request="'${HEIGHT_CHECKBOX}'">
  <checkbox active="'${CODECS}'" sensitive="'${CHECKBOX_CODECS_ACTIF}'" tooltip-text="'${comment_codecs}'">
    <variable>CODECS</variable>
    <label>'${msg_codecs}'</label>
  </checkbox>
  <checkbox active="'${ARIAL_FONTS}'" sensitive="'${CHECKBOX_ARIAL_FONTS_ACTIF}'"  visible="'${ARIAL_FONTS_VISIBLE}'" tooltip-text="'${comment_arial_fonts}'">
    <variable>ARIAL_FONTS</variable>
    <label>'${msg_arial_fonts}'</label>
  </checkbox>
  <checkbox active="'${CALIBRI_FONTS}'" sensitive="'${CHECKBOX_CALIBRI_FONTS_ACTIF}'"  visible="'${CALIBRI_FONTS_VISIBLE}'" tooltip-text="'${comment_calibri_fonts}'">
    <variable>CALIBRI_FONTS</variable>
    <label>'${msg_calibri_fonts}'</label>
  </checkbox>
</vbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <hbox spacing="10" space-expand="false" space-fill="true">
 <button cancel></button>
 <button can-default="true" has-default="true" use-stock="true" is-focus="true">
 <label>gtk-ok</label>
 <action>exit:OK</action>
 </button>
 </hbox>

</vbox>
</window>'


MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE="$(gtkdialog --center --program=WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE)"

eval ${MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE}

echo "MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE=${MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE}"


if [[ ${CODECS} == "true" && ${CHECKBOX_CODECS_ACTIF} == "true" ]]
then
    choix=${choix}:Codecs
fi

if [[ ${ARIAL_FONTS} == "true" && ${CHECKBOX_ARIAL_FONTS_ACTIF} == "true" ]]
then
    choix=${choix}:Arial
fi

if [[ ${CALIBRI_FONTS} == "true" && ${CHECKBOX_CALIBRI_FONTS_ACTIF} == "true" ]]
then
    choix=${choix}:Calibri
fi


echo "choix=${choix}"


if [ ${EXIT} == "OK" ]
then

    if [[ ${choix} != "" ]]
    then

        user=$USER

        pkexec $dir_install_non_free_softwares/$nom_script "$choix" "${user}"

    fi
fi



